# ************************************************************
# Sequel Pro SQL dump
# Version 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: 127.0.0.1 (MySQL 5.1.44)
# Database: bt_test
# Generation Time: 2013-12-19 10:57:26 +1300
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table pages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pages`;

CREATE TABLE `pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(15) NOT NULL,
  `title` varchar(100) NOT NULL DEFAULT '',
  `heading` varchar(100) NOT NULL,
  `description` varchar(255) NOT NULL,
  `keywords` varchar(255) DEFAULT NULL,
  `content` text,
  `last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;

INSERT INTO `pages` (`id`, `slug`, `title`, `heading`, `description`, `keywords`, `content`, `last_updated`)
VALUES
	(1,'home','Home','Welcome To STP Framework','STP Framework is built with Slim, Twig and provides an example model class that uses PDO.','slim, twig, pdo, php, stp','STP Framework is built with <a href=\"http://slimframework.com\">Slim</a>, <a href=\"http://twig.sensiolabs.org\">Twig</a> and provides an example model class that uses <a href=\"http://php.net/manual/en/book.pdo.php\">PHP Data Objects (PDO)</a>.\r\n\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac posuere odio. Duis eu urna posuere, auctor est eu, commodo libero. Praesent et ipsum risus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Curabitur ac elit at mauris sollicitudin blandit. Quisque ante neque, molestie a lacinia feugiat, iaculis ac risus. Duis fermentum porttitor nunc, quis pulvinar dui fermentum tempor. Suspendisse sed sapien aliquet, congue nibh quis, cursus erat. Sed hendrerit elit elit, ut accumsan velit pellentesque nec. Suspendisse iaculis nisi dolor, eget scelerisque purus volutpat quis. Nam consequat ipsum lectus, at ornare ipsum porttitor nec. Sed volutpat aliquam lacinia. Pellentesque enim turpis, dictum vitae diam at, porttitor consectetur elit.\r\n\r\nVestibulum mi tellus, viverra quis ornare a, vulputate vel nisi. Cras sed pretium urna. Mauris nibh massa, mattis quis fringilla aliquet, bibendum sit amet urna. Pellentesque id porttitor arcu. Nulla a orci lectus. Sed a dapibus nisl. Mauris et enim nec augue condimentum elementum at ut leo. Etiam tempor tincidunt aliquam. Integer tempor suscipit neque, at viverra ligula cursus vitae. Nunc id eros in risus tincidunt sollicitudin ac id augue. Nunc sit amet augue id arcu sollicitudin posuere sed eu dolor.\r\n\r\nEtiam iaculis mi sem, quis fringilla augue consequat ut. Donec ullamcorper vitae risus sed tincidunt. Nam vulputate semper purus at fringilla. Cras volutpat velit posuere eros posuere venenatis. Sed laoreet felis pharetra varius lacinia. Curabitur aliquam dui ornare volutpat euismod. Aliquam vitae vulputate lectus. Phasellus urna orci, volutpat at metus mattis, consequat adipiscing sem. Integer porta, ipsum vitae interdum blandit, arcu massa sagittis lacus, at convallis nisl magna tristique mauris. Sed semper ac est sed faucibus. Donec viverra neque mi, sit amet pretium metus bibendum quis. Mauris a facilisis dui.\r\n\r\nPhasellus viverra at lacus ac posuere. Sed venenatis ligula sit amet nisl convallis, vel vulputate odio convallis. Sed iaculis elementum risus, eget ultrices nunc. Curabitur quam quam, scelerisque non dui id, consectetur hendrerit leo. Nam id imperdiet lorem. Vestibulum tincidunt sit amet dui dignissim commodo. Praesent vel condimentum tortor. Proin pulvinar ipsum ac velit condimentum consequat. Donec accumsan lacus eu viverra placerat.\r\n\r\nDonec posuere, diam vitae condimentum pharetra, massa lectus vehicula risus, sit amet adipiscing diam arcu a ligula. Nulla fermentum augue eu turpis luctus, vel facilisis ipsum posuere. Donec lacus neque, consequat in ultrices vel, faucibus sed quam. Suspendisse vel nisi blandit, ornare lacus id, viverra mauris. Praesent commodo odio in dapibus rhoncus. Duis eu risus dolor. Pellentesque sed ultrices risus, id eleifend neque.','2013-12-19 10:53:13'),
	(2,'apples','Apples','Apples and Oranges','A comparison of apples and oranges occurs when two items or groups of items are compared that cannot be practically compared. The idiom, comparing apples and oranges, refers to the apparent differences between items which are popularly thought to be incom','apples, oranges','A comparison of apples and oranges occurs when two items or groups of items are compared that cannot be practically compared.\r\n\r\nThe idiom, comparing apples and oranges, refers to the apparent differences between items which are popularly thought to be incomparable or incommensurable, such as apples and oranges. The idiom may also be used to indicate that a false analogy has been made between two items, such as where an apple is faulted for not being a good orange.','2013-12-19 10:57:14');

/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
