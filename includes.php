<?php

require_once "vendor/Slim/Slim/Slim/Slim.php";
\Slim\Slim::registerAutoloader();

require_once "vendor/Twig/Twig/lib/Twig/Autoloader.php";
Twig_Autoloader::register();

require_once "vendor/Slim/Views/Twig.php";
require_once "vendor/Slim/Views/TwigExtension.php";

require_once "class/Twig_Extension_Nl2Para.php";

require_once "model/Database.php";
require_once "model/Model.php";

require_once "class/Validation.php";

require_once "model/PageModel.php";