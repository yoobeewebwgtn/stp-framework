<?php

ini_set('default_charset', 'utf-8');

require "config.php";

require "includes.php";

//Instantiate a Slim application:
$app = new \Slim\Slim(array(
    'view' => new \Slim\Views\Twig(),
    'templates.path' => 'template',
    'debug' => true
));

$view = $app->view();

$view->parserOptions = array(
    'debug' => true,
    //'cache' => 'template_cache'
);

$view->parserExtensions = array(
    new \Slim\Views\TwigExtension(),
    new Twig_Extension_Debug(),
    new Twig_Extension_Nl2Para()
);

// TWIG GLOBAL VARIABLES

$twig = $view->getInstance();
$twig->addGlobal('site', array(
    'root' => SITE_ROOT,
    'title' => SITE_TITLE
    )
);

// ROUTES

require 'routes.php';


// DO EVERYTHING

//Run the Slim application:
$app->run();

