<?php

if ($_SERVER['HTTP_HOST'] == "localhost") {
    $host = "dev";
} else {
    $host = "live";
}

/* --- */

switch ($host) {
    case "dev":
        define('HOST_TYPE', 'dev');
        error_reporting(E_ALL);
        define('DB_DSN', 'mysql:dbname=database_name;host=localhost;charset=UTF8');
        define('DB_USER', 'root');
        define('DB_PASS', '');
        define('SITE_ROOT', '/~brett.taylor/stp-framework/public/'); // trailing slash
        break;

    case "live":
        // no break
    default:
        define('HOST_TYPE', 'live');
        define('DB_DSN', 'mysql:dbname=database_name;host=localhost;charset=UTF8');
        define('DB_USER', 'brettt_stp');
        define('DB_PASS', 'password');
        define('SITE_ROOT', '/stp-framework/public/'); // trailing slash
        break;

}

define("SITE_TITLE", "STP Framework Template");

define('PATH_TEMPLATE', 'lib/template');
