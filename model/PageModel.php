<?php

class PageModel extends Model
{

    public $allowedData = array(
        'id'            => array(
            'actions'       => array('edit'),
            'validate'      => 'Validation::checkNumeric',
        ),
        'slug'          => array(
            'validate'      => 'PageModel::validateSlug',
            'actions'       => array('add'),
        ),
        'title'         => array(
            'validate'      => 'Validation::checkRequired',
        ),
        'heading'       => array(
            'validate'      => 'Validation::checkRequired',
        ),
        'description'   => array(
            'validate'      => 'Validation::checkRequired',
        ),
        'keywords'      => array(
            'validate'      => NULL, // not required
        ),
        'content'       => array(
            'validate'      => 'Validation::checkRequired',
        ),
        'last_updated'  => array(
            'validate'      => NULL,
            'actions'       => array() // never retain this field.
        )
    );

    public function __construct($slug = null)
    {
        parent::__construct();
        if (!is_null($slug)) {
            $this->getOneFromSlug($slug);
        }
    }

    public function findAll()
    {
        $statement = $this->db->prepare("SELECT id, slug, title, heading FROM pages ORDER BY last_updated");
        $this->db->executeStatement($statement);
        return $statement->fetchAll();
    }

    public function find($id)
    {
        $statement = $this->db->prepare("SELECT * FROM pages WHERE id = :id");
        $statement->bindValue(":id", $id);
        $this->db->executeStatement($statement);
        return $statement->fetch();
    }

    public function getOneFromSlug($slug)
    {
        $this->data = $this->selectSlug($slug);
        return $this->data;
    }

    public function insert()
    {
        $query  = "INSERT INTO pages (slug, title, heading, description, keywords, content) "
                . "VALUES (:slug , :title , :heading , :description , :keywords , :content );";
        $statement = $this->db->prepare($query);
        foreach ($this->data as $key => $value) {
            if ($this->isUsableInAction('add', $key)) {
                $statement->bindValue(":" . $key, $value);
            }
        }
        //$statement->debugDumpParams();
        $this->db->executeStatement($statement); // if insert fails, it will throw error
        return $this->db->lastInsertId();
    }


    public function update()
    {
        $query  = "UPDATE pages "
                . " SET title = :title , heading= :heading , "
                . "     description = :description , keywords = :keywords , content = :content "
                . "WHERE id = :id;";
        $statement = $this->db->prepare($query);
        foreach ($this->data as $key => $value) {
            if ($this->isUsableInAction('edit', $key)) {
                $statement->bindValue(":" . $key, $value);
            }
        }
        //$statement->debugDumpParams();
        $this->db->executeStatement($statement); // if insert fails, it will throw error
        return $this->data['id'];
    }

    public function delete()
    {
        $statement = $this->db->prepare("DELETE FROM pages WHERE id = :id");
        $statement->bindValue(":id", $this->id());
        return $this->db->executeStatement($statement);
    }


    public static function validateSlug($slug)
    {
        $message = Validation::checkRequired($slug);
        if (strlen($message)>0) {
            return $message;
        }
        // make sure not present in database
        $record = new self();
        $record = $record->selectSlug($slug);
        if (in_array($record['slug'], array('new'))) {
            $message = "Cannot use that slug.";
        }
        if ($record['slug'] == $slug) {
            $message = "A page with that slug already exists.";
        }
        return $message;
    }

    public function selectSlug($slug)
    {
        $statement = $this->db->prepare("SELECT * FROM pages WHERE slug = :slug");
        $statement->bindValue(":slug", $slug);
        $this->db->executeStatement($statement);
        return $statement->fetch();
    }

}