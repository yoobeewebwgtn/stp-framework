<?php

abstract class Model
{
    protected $db;

    public $data = array();
    public $messages = array();

    //abstract public $allowedData;

    public $idField = 'id';

    public function __construct()
    {
        $this->connectToDatabase();
    }

    public function connectToDatabase()
    {
        $this->db = new Database();
    }

    abstract protected function find($id);
    abstract protected function findAll();

    abstract protected function insert();
    abstract protected function update();
    abstract protected function delete();


    public function save()
    {
        if (!$this->id()) {
            return $this->insert();
        } else {
            return $this->update();
        }
    }

    public function remove()
    {
        if (!$this->id()) {
            throw new UnexpectedValueException('Id not provided.');
        }
        return $this->delete();
    }

    public function id()
    {
        if (!isset($this->data[$this->idField])) return false;
        return $this->data[$this->idField];
    }

    public function processInput($input)
    {
        // check for form submission
        $this->parseInput($input);
        $isValid = $this->validate($input['action']);
        if ($isValid) {
            // commit to database if no problems
            return $this->save();
        }
        return false;
    }

    public function parseInput( $input = array() )
    {
        // populate $this->data with the input data
        foreach ($input as $key => $value) {
            // for each field of input, see if this object should keep it
            if (array_key_exists($key, $this->allowedData)) {
                $this->data[$key] = $value;
            }
        }
    }

    public function validate($action = "add")
    {
        // validate form, setting $this->messages for view to render.

        // take each piece of data stored in this object
        foreach ($this->data as $key => $value) {
            if (!$this->isUsableInAction($action, $key)) {
                // discard the data if we shouldn't be using it in this action (ie, add, edit)
                unset($this->data[$key]);
            } else if (isset($this->allowedData[$key]) && !is_null($this->allowedData[$key]['validate'])) {
                // if this data is allowed, and has a validate option...

                // check to see if this validate option is actually a callable method/function
                if (!is_callable($this->allowedData[$key]['validate'])){
                    throw new Exception("found uncallable validate callback on $key");
                }
                // validate the data, and capture the returned message, if any.
                $this->messages[$key] = call_user_func($this->allowedData[$key]['validate'], $value);
            }
        }
        //print_r($this->data);
        return Validation::checkErrorMessages($this->messages);
    }

    public function isUsableInAction($action, $key)
    {
        /*
         * if this key is present in $allowedData, and:
         *  ... doesn't have an actions property OR
         *  ... has an actions property that contains $action
         * return true.
         */
        return (!isset($this->allowedData[$key]['actions']) || (in_array($action, $this->allowedData[$key]['actions'])));
    }

}