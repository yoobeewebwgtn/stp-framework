<?php

// NEW PAGE

$app->get('/', function () use ($app)
{
    $app->render('home.twig');
});

$app->get('/page/new', function () use ($app)
{
    $app->render('pageEdit.twig', array(
        'mode' => 'add',
        'page' => array()
    ));
});

// HANDLE NEW PAGE
$app->post('/page/new', function () use ($app)
{
    $page = new PageModel();
    $result = $page->processInput($_POST);
    if (!$result) {
        $app->flashNow('error', 'There are some errors with your form.');
        $app->render('pageEdit.twig', array(
            'mode' => 'add',
            'page' => $page->data,
            'messages' => $page->messages
        ));
    } else {
        $app->flash('success', 'Page added.');
        $app->redirect(SITE_ROOT . 'page/' . $page->data['slug']);
    }
});

// EDIT PAGE
$app->get('/page/:slug/edit', function ($slug) use ($app)
{
    $page = new PageModel($slug);
    if (!$page->id()) {
        $app->pass(); // this page does not exist, pass it on.
    }
    $app->render('pageEdit.twig', array(
        'mode' => 'edit',
        'page' => $page->data
    ));
});

// HANDLE EDIT PAGE

$app->post('/page/:slug/edit', function ($slug) use ($app)
{
    $page = new PageModel($slug);
    if (!$page->id()) {
        $app->pass(); // this page does not exist, pass it on.
    }
    if (!$page->processInput($_POST)) {
        $app->flashNow('error', 'There are some errors with your form.');
        $app->render('pageEdit.twig', array(
            'mode' => 'edit',
            'page' => $page->data,
            'messages' => $page->messages
        ));
    } else {
        $app->flash('success', 'Page updated.');
        $app->redirect(SITE_ROOT . 'page/' . $slug);
    }
});

// DELETE PAGE
$app->get('/page/:slug/delete', function ($slug) use ($app)
{
    $page = new PageModel($slug);
    if (!$page->id()) {
        $app->pass(); // this page does not exist, pass it on.
    }
    $app->render('pageDelete.twig', array(
        'page' => $page->data
    ));
});

// HANDLE DELETE PAGE

$app->post('/page/:slug/delete', function ($slug) use ($app)
{
    $page = new PageModel($slug);
    if (!$page->id()) {
        $app->pass(); // this page does not exist, pass it on.
    }
    if (!$page->delete()) {
        $app->flash('error', "Couldn't delete this post.");
        $app->redirect(SITE_ROOT . 'page/' . $page->data['slug'] . '/edit');
    } else {
        $app->flash('success', 'Page updated.');
        $app->redirect(SITE_ROOT . 'page/');
    }
});


// VIEW PAGE
$app->get('/page/:slug', function ($slug) use ($app)
{
    $page = new PageModel($slug);
    if (!$page->id()) {
        $app->pass(); // this page does not exist, pass it on.
    }
    $app->render('page.twig', array('page' => $page->data));
});


// VIEW PAGES

$app->get('/page/?', function () use ($app) // ? -> trailing slash is optional
{
    $pages = new PageModel();
    $pages = $pages->findAll();
    $app->render('pageList.twig', array('pages' => $pages));
});




/* **************************** */

$app->notFound(function () use ($app)
{
    $app->render('notFound.twig');
});

$app->error(function (Exception $e) use ($app)
{
    echo "<hr>";
    echo "<h3>PHP Error</h3>";
    if (HOST_TYPE == "dev") {
        echo "<h4>" . $e->getMessage() . "</h4>";
        echo "<pre>";
        var_dump($e);
        echo "</pre>";
    }
    echo "<hr>";
});