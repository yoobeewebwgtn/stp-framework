<?php
/**
 * Provides twig filters for outputting text broken up by new lines into elements.
 *
 * {{ text|nl2p|raw }} will output text, wrapping each line in paragraphs. Blank lines are stripped.
 *
 * @author Brett Taylor <brett.taylor@yoobee.ac.nz>
 *
 */

class Twig_Extension_Nl2Para extends Twig_Extension {

    public function getName ()
    {
        return 'Twig_Extension_Nl2Para';
    }

    public function getFilters()
    {
        return array(
            'nl2para' => new Twig_Filter_Method($this, 'newLinesToParagraphElements'),
            'nl2p' => new Twig_Filter_Method($this, 'newLinesToParagraphElements'),
            'nl2li' => new Twig_Filter_Method($this, 'newLinesToListElements'),
            'nl2el' => new Twig_Filter_Method($this, 'newLinesToElements')
        );
    }

    public static function newLinesToParagraphElements($content)
    {
        return self::wrapNewLinesInStrings($content, '<p>', '</p>');
    }

    public static function newLinesToListElements($content)
    {
        return self::wrapNewLinesInStrings($content, '<li>', '</li>');
    }

    public static function newLinesToElements($content, $tagName = "li")
    {
        return self::wrapNewLinesInStrings($content, '<' . $tagName . '>', '</'. $tagName .'>');
    }


    public static function wrapNewLinesInStrings($content, $openTag = "<li>", $closeTag = "</li>")
    {
        $paragraphs = "";
        preg_match_all("/^(.+)$/m", $content, $lines);
        foreach ($lines[0] as $line) {
            $line = trim($line);
            if (strlen($line) > 0) {
                $paragraphs .= $openTag . $line . $closeTag . "\n";
            }
        }
        return $paragraphs;
    }

}